package com.zainco.mitchuitestingcourse

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {
    /*  @get:Rule
      var activityRule: ActivityScenarioRule<MainActivity>
              = ActivityScenarioRule(MainActivity::class.java)*/

    @Test
    fun test_isActivityInView() {//activity displayed
        //to launch actiivy in espresso, we need to use activity scenario
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.main)).check(matches(isDisplayed()))

    }

    @Test
    fun test_visibility_title_next_buttons() {
        //to launch actiivy in espresso, we need to use activity scenario
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.button)).check(matches(isDisplayed()))// method 1
        onView(withId(R.id.button)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))//method 2

    }
//will removed
    @Test
    fun ss(){

    }
    @Test
    fun test_isTitleTextDisplayed() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.activity_main_title)).check(matches(withText(R.string.text_mainactivity)))
    }


    @Test
    fun test_navSecondaryActivity() {
        val activityScenario =ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.button)).perform(click())
        onView(withId(R.id.secondary)).check(matches(isDisplayed()))
    }
    @Test
    fun test_pressing_backbutton() {
        val activityScenario =ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.button)).perform(click())
        onView(withId(R.id.secondary)).check(matches(isDisplayed()))
//        onView(withId(R.id.button_back)).perform(click()) //method 1
        pressBack()//method 2
        onView(withId(R.id.main)).check(matches(isDisplayed()))
    }
}